<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Http\Request;

Route::get('/', function () {
		$brick_x = \App\Brick::orderBy('created_at', 'asc') -> get();
	      return view('brick'); ['brick' => $brick ];

});


Route::post('/brick', function (Request $request) {
		$validator = Validator::make($request->all(), [
			'brick' => 'required|integer' /*, 'brick_y' => 'required|integer', 'brick_z' => 'required|integer', 'hole_x' => 'required|integer', 'hole_y' => 'required|integer' */
		]);

		if ($validator -> fails()) {
	  		return redirect('/')
	  		-> withInput()
	  		-> withErrors ($validator);
	}

	$brick = new \App\Brick; 
	$brick -> brick = $request-> brick;
	$brick -> save();

	return redirect ('/');

});

