<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brick', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brick'); /*
            $table->integer('brick_y');
            $table->integer('brick_z');
            $table->integer('hole_x');
            $table->integer('hole_y');
            $table->text('status'); */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brick');
    }
}
