@extends('layouts.app')

@section('content')

<div class="card-body">
	@include('errors')
	<form action="{{url('brick')}}" method="POST" class="form-horizontal">
		{{csrf_field () }}

		<div class="row">
			<div class="form-group">
				<label for="Brick" class="col-sm-3" control-label">Brick</label>
				<div class="row">
					<div class="col-sm-6">
						<input type="text" name="brick" id="brick" class="form-control">
					</div>
					<div class="col-sm-6">
						<button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Do it </button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>




@if(count($brick) > 0)
<div class="card">
	<div class="card-heading">
		Previous bricks
	</div>
	<div class="card-body">
		<table class="table table-striped brick-table">
			<thead>
				<th>List</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
				@foreach($bricks as $brick)

					<td class="table-text">
					//	<div>{{$brick -> brick}}</div>
					</td>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endif

@endsection



